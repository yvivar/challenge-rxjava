package com.challenge.rxjava.controller;

import com.challenge.rxjava.dto.StudentDtoCreate;
import com.challenge.rxjava.dto.StudentDtoUpdate;
import com.challenge.rxjava.model.Constants;
import com.challenge.rxjava.model.Student;
import com.challenge.rxjava.services.IStudentService;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@ApiModel(value = Constants.API_CONTROLLER_STUDENT)
@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private IStudentService studentService;

    @ApiOperation(value = Constants.API_OPERATION_GET_GETSTUDENT_VALUE,
                  httpMethod = Constants.API_OPERATION_GET_GETSTUDENT_HTTPMETHOD,
                  notes = Constants.API_OPERATION_GET_GETSTUDENT_NOTES)
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Single<ResponseEntity<Student>> getStudent(@PathVariable Long id) {
        return studentService.getStudent(id).map(ResponseEntity::ok);
    }

    @ApiOperation(value = Constants.API_OPERATION_GET_GETALLSTUDENT_VALUE,
                  httpMethod = Constants.API_OPERATION_GET_GETALLSTUDENT_HTTPMETHOD,
                  notes = Constants.API_OPERATION_GET_GETALLSTUDENT_NOTES)
    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public Single<ResponseEntity<List<Student>>> getAllStudent() {
        return studentService.getListStudents().map(ResponseEntity::ok);
    }

    @ApiOperation(value = Constants.API_OPERATION_POST_ADDSTUDENT_VALUE,
                  httpMethod = Constants.API_OPERATION_POST_ADDSTUDENT_HTTPMETHOD,
                  notes = Constants.API_OPERATION_POST_ADDSTUDENT_NOTES)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
                 produces = MediaType.APPLICATION_JSON_VALUE)
    public Single<ResponseEntity<Student>> addStudent(@RequestBody @Validated StudentDtoCreate student) {
        return studentService.addStudent(student).map(ResponseEntity::ok);
    }

    @ApiOperation(value = Constants.API_OPERATION_PUT_UPDATESTUDENT_VALUE,
                  httpMethod = Constants.API_OPERATION_PUT_UPDATESTUDENT_HTTPMETHOD,
                  notes = Constants.API_OPERATION_PUT_UPDATESTUDENT_NOTES)
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
                produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Completable updateStudent(@RequestBody @Validated StudentDtoUpdate student) {
        return studentService.updateStudent(student);
    }

    @ApiOperation(value = Constants.API_OPERATION_DELETE_DELETESTUDENT_VALUE,
                  httpMethod = Constants.API_OPERATION_DELETE_DELETESTUDENT_HTTPMETHOD,
                  notes = Constants.API_OPERATION_DELETE_DELETESTUDENT_NOTES)
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Completable deleteStudent(@PathVariable Long id) {
        return studentService.deleteStudent(id);
    }

}
