package com.challenge.rxjava.controller;

import com.challenge.rxjava.dto.ErrorMessageDto;
import com.challenge.rxjava.exception.StudentNotFoundException;
import com.challenge.rxjava.model.Constants;
import io.swagger.annotations.ApiModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolationException;
import java.util.Date;

@ApiModel(value = Constants.API_CONTROLLER_ADVICE)
@RestControllerAdvice
public class AdviceController {

    @ExceptionHandler(StudentNotFoundException.class)
    public ResponseEntity<ErrorMessageDto> handleEntityNotFoundException(StudentNotFoundException e) {
        ErrorMessageDto errorMessage = new ErrorMessageDto(HttpStatus.NOT_FOUND.value(), new Date(), e.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(errorMessage);
    }

    @ExceptionHandler({
            MethodArgumentNotValidException.class,
            MethodArgumentTypeMismatchException.class,
            ConstraintViolationException.class,
            HttpMessageConversionException.class
    })
    public ResponseEntity<ErrorMessageDto> handleArgumentException(Exception e) {
        ErrorMessageDto errorMessage = new ErrorMessageDto(HttpStatus.BAD_REQUEST.value(), new Date(), e.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(errorMessage);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorMessageDto> globalExceptionHandler(Exception e) {
        ErrorMessageDto errorMessage = new ErrorMessageDto(HttpStatus.INTERNAL_SERVER_ERROR.value(), new Date(), e.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(errorMessage);
    }

}
