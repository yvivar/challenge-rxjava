package com.challenge.rxjava.model;

public enum Status {
    ACTIVE("ACTIVE"), INACTIVE("INACTIVE");
    public String statusDescription;
    Status(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String toString() {
        return String.valueOf(statusDescription);
    }
}
