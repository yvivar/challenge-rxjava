package com.challenge.rxjava.model;

import com.challenge.rxjava.dto.StudentDtoCreate;
import com.challenge.rxjava.dto.StudentDtoUpdate;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.io.Serializable;

@ApiModel(value = Constants.API_MODEL_STUDENT)
@Entity(name = Constants.MODEL_STUDENT_ENTITY)
@Table(name = Constants.MODEL_STUDENT_TABLE)
public class Student implements Serializable {

    @ApiModelProperty(position = Constants.API_MODEL_PROPERTY_STUDENT_MODEL_ID_POSITION,
                      value = Constants.API_MODEL_PROPERTY_STUDENT_MODEL_ID_VALUE,
                      dataType = Constants.API_MODEL_PROPERTY_STUDENT_MODEL_ID_DATATYPE,
                      required = Constants.API_MODEL_PROPERTY_STUDENT_MODEL_ID_REQUIRED)
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ApiModelProperty(position = Constants.API_MODEL_PROPERTY_STUDENT_MODEL_FIRST_NAME_POSITION,
                      value = Constants.API_MODEL_PROPERTY_STUDENT_MODEL_FIRST_NAME_VALUE,
                      dataType = Constants.API_MODEL_PROPERTY_STUDENT_MODEL_FIRST_NAME_DATATYPE,
                      required = Constants.API_MODEL_PROPERTY_STUDENT_MODEL_FIRST_NAME_REQUIRED)
    @Column(name = "first_name")
    private String firstName;

    @ApiModelProperty(position = Constants.API_MODEL_PROPERTY_STUDENT_MODEL_LAST_NAME_POSITION,
                      value = Constants.API_MODEL_PROPERTY_STUDENT_MODEL_LAST_NAME_VALUE,
                      dataType = Constants.API_MODEL_PROPERTY_STUDENT_MODEL_LAST_NAME_DATATYPE,
                      required = Constants.API_MODEL_PROPERTY_STUDENT_MODEL_LAST_NAME_REQUIRED)
    @Column(name = "last_name")
    private String lastName;


    @ApiModelProperty(position = Constants.API_MODEL_PROPERTY_STUDENT_MODEL_EMAIL_POSITION,
                      value = Constants.API_MODEL_PROPERTY_STUDENT_MODEL_EMAIL_VALUE,
                      dataType = Constants.API_MODEL_PROPERTY_STUDENT_MODEL_EMAIL_DATATYPE,
                      required = Constants.API_MODEL_PROPERTY_STUDENT_MODEL_EMAIL_REQUIRED)
    @Column(name = "email")
    private String email;

    @ApiModelProperty(position = Constants.API_MODEL_PROPERTY_STUDENT_MODEL_STATUS_POSITION,
                      value = Constants.API_MODEL_PROPERTY_STUDENT_MODEL_STATUS_VALUE,
                      dataType = Constants.API_MODEL_PROPERTY_STUDENT_MODEL_STATUS_DATATYPE,
                      required = Constants.API_MODEL_PROPERTY_STUDENT_MODEL_STATUS_REQUIRED)
    @Column(name = "status")
    private String status;

    public static Student getStudentCreation(StudentDtoCreate studentDto) {
        Student student = new Student();
        student.setFirstName(studentDto.getFirstName());
        student.setLastName(studentDto.getLastName());
        student.setEmail(studentDto.getEmail());
        student.setStatus(Status.ACTIVE.toString());
        return student;
    }

    public static Student getStudentUpdate(StudentDtoUpdate studentDto) {
        Student student = new Student();
        student.setId(studentDto.getId());
        student.setFirstName(studentDto.getFirstName());
        student.setLastName(studentDto.getLastName());
        student.setEmail(studentDto.getEmail());
        student.setStatus(Status.ACTIVE.toString());
        return student;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private static final long serialVersionUID = 1285454306356845810L;
}
