package com.challenge.rxjava.model;

import io.swagger.annotations.ApiModel;

@ApiModel(value = Constants.CONSTANTS_MODEL)
public class Constants {

    public final static String CONSTANTS_MODEL = "Constants";

    public final static String API_MODEL_STUDENT = "Student";

    public final static String API_DTO_ERRORMESSAGEDTO = "ErrorMessageDto";
    public final static String API_DTO_STUDENTDTOCREATE = "StudentDtoCreate";
    public final static String API_EXCEPTION_STUDENTNOTFOUNDEXCEPTION = "StudentNotFoundException";
    public final static String API_CONTROLLER_STUDENT = "StudentController";
    public final static String API_CONTROLLER_ADVICE = "AdviceController";

    public final static int API_MODEL_PROPERTY_STUDENT_MODEL_ID_POSITION = 1;
    public final static String API_MODEL_PROPERTY_STUDENT_MODEL_ID_VALUE = "id";
    public final static String API_MODEL_PROPERTY_STUDENT_MODEL_ID_DATATYPE = "Long";
    public final static boolean API_MODEL_PROPERTY_STUDENT_MODEL_ID_REQUIRED = true;

    public final static int API_MODEL_PROPERTY_STUDENT_MODEL_FIRST_NAME_POSITION = 2;
    public final static String API_MODEL_PROPERTY_STUDENT_MODEL_FIRST_NAME_VALUE = "first_name";
    public final static String API_MODEL_PROPERTY_STUDENT_MODEL_FIRST_NAME_DATATYPE = "String";
    public final static boolean API_MODEL_PROPERTY_STUDENT_MODEL_FIRST_NAME_REQUIRED = true;

    public final static int API_MODEL_PROPERTY_STUDENT_MODEL_LAST_NAME_POSITION = 3;
    public final static String API_MODEL_PROPERTY_STUDENT_MODEL_LAST_NAME_VALUE = "last_name";
    public final static String API_MODEL_PROPERTY_STUDENT_MODEL_LAST_NAME_DATATYPE = "String";
    public final static boolean API_MODEL_PROPERTY_STUDENT_MODEL_LAST_NAME_REQUIRED = true;

    public final static int API_MODEL_PROPERTY_STUDENT_MODEL_EMAIL_POSITION = 4;
    public final static String API_MODEL_PROPERTY_STUDENT_MODEL_EMAIL_VALUE = "email";
    public final static String API_MODEL_PROPERTY_STUDENT_MODEL_EMAIL_DATATYPE = "String";
    public final static boolean API_MODEL_PROPERTY_STUDENT_MODEL_EMAIL_REQUIRED = true;

    public final static int API_MODEL_PROPERTY_STUDENT_MODEL_STATUS_POSITION = 5;
    public final static String API_MODEL_PROPERTY_STUDENT_MODEL_STATUS_VALUE = "status";
    public final static String API_MODEL_PROPERTY_STUDENT_MODEL_STATUS_DATATYPE = "String";
    public final static boolean API_MODEL_PROPERTY_STUDENT_MODEL_STATUS_REQUIRED = true;

    public final static int API_MODEL_PROPERTY_ERRORMESSAGEDTO_DTO_STATUS_POSITION = 1;
    public final static String API_MODEL_PROPERTY_ERRORMESSAGEDTO_DTO_STATUS_VALUE = "statusCode";
    public final static String API_MODEL_PROPERTY_ERRORMESSAGEDTO_DTO_STATUS_DATATYPE = "String";
    public final static boolean API_MODEL_PROPERTY_ERRORMESSAGEDTO_DTO_STATUS_REQUIRED = true;

    public final static int API_MODEL_PROPERTY_ERRORMESSAGEDTO_DTO_CURRENTDATE_POSITION = 2;
    public final static String API_MODEL_PROPERTY_ERRORMESSAGEDTO_DTO_CURRENTDATE_VALUE = "currentDate";
    public final static String API_MODEL_PROPERTY_ERRORMESSAGEDTO_DTO_CURRENTDATE_DATATYPE = "String";
    public final static boolean API_MODEL_PROPERTY_ERRORMESSAGEDTO_DTO_CURRENTDATE_REQUIRED = true;

    public final static int API_MODEL_PROPERTY_ERRORMESSAGEDTO_DTO_MESSAGEERROR_POSITION = 3;
    public final static String API_MODEL_PROPERTY_ERRORMESSAGEDTO_DTO_MESSAGEERROR_VALUE = "messageError";
    public final static String API_MODEL_PROPERTY_ERRORMESSAGEDTO_DTO_MESSAGEERROR_DATATYPE = "String";
    public final static boolean API_MODEL_PROPERTY_ERRORMESSAGEDTO_DTO_MESSAGEERROR_REQUIRED = true;

    public final static int API_MODEL_PROPERTY_STUDENTDTOCREATE_DTO_FIRSTNAME_POSITION = 1;
    public final static String API_MODEL_PROPERTY_STUDENTDTOCREATE_DTO_FIRSTNAME_VALUE = "firstName";
    public final static String API_MODEL_PROPERTY_STUDENTDTOCREATE_DTO_FIRSTNAME_DATATYPE = "String";
    public final static boolean API_MODEL_PROPERTY_STUDENTDTOCREATE_DTO_FIRSTNAME_REQUIRED = true;

    public final static int API_MODEL_PROPERTY_STUDENTDTOCREATE_DTO_LASTNAME_POSITION = 2;
    public final static String API_MODEL_PROPERTY_STUDENTDTOCREATE_DTO_LASTNAME_VALUE = "lastName";
    public final static String API_MODEL_PROPERTY_STUDENTDTOCREATE_DTO_LASTNAME_DATATYPE = "String";
    public final static boolean API_MODEL_PROPERTY_STUDENTDTOCREATE_DTO_LASTNAME_REQUIRED = true;

    public final static int API_MODEL_PROPERTY_STUDENTDTOCREATE_DTO_EMAIL_POSITION = 3;
    public final static String API_MODEL_PROPERTY_STUDENTDTOCREATE_DTO_EMAIL_VALUE = "email";
    public final static String API_MODEL_PROPERTY_STUDENTDTOCREATE_DTO_EMAIL_DATATYPE = "String";
    public final static boolean API_MODEL_PROPERTY_STUDENTDTOCREATE_DTO_EMAIL_REQUIRED = true;

    public final static int API_MODEL_PROPERTY_STUDENTDTOUPDATE_DTO_ID_POSITION = 1;
    public final static String API_MODEL_PROPERTY_STUDENTDTOUPDATE_DTO_ID_VALUE = "id";
    public final static String API_MODEL_PROPERTY_STUDENTDTOUPDATE_DTO_ID_DATATYPE = "Long";
    public final static boolean API_MODEL_PROPERTY_STUDENTDTOUPDATE_DTO_ID_REQUIRED = true;

    public final static int API_MODEL_PROPERTY_STUDENTDTOUPDATE_DTO_FIRSTNAME_POSITION = 2;
    public final static String API_MODEL_PROPERTY_STUDENTDTOUPDATE_DTO_FIRSTNAME_VALUE = "firstName";
    public final static String API_MODEL_PROPERTY_STUDENTDTOUPDATE_DTO_FIRSTNAME_DATATYPE = "String";
    public final static boolean API_MODEL_PROPERTY_STUDENTDTOUPDATE_DTO_FIRSTNAME_REQUIRED = true;

    public final static int API_MODEL_PROPERTY_STUDENTDTOUPDATE_DTO_LASTNAME_POSITION = 3;
    public final static String API_MODEL_PROPERTY_STUDENTDTOUPDATE_DTO_LASTNAME_VALUE = "lastName";
    public final static String API_MODEL_PROPERTY_STUDENTDTOUPDATE_DTO_LASTNAME_DATATYPE = "String";
    public final static boolean API_MODEL_PROPERTY_STUDENTDTOUPDATE_DTO_LASTNAME_REQUIRED = true;

    public final static int API_MODEL_PROPERTY_STUDENTDTOUPDATE_DTO_EMAIL_POSITION = 4;
    public final static String API_MODEL_PROPERTY_STUDENTDTOUPDATE_DTO_EMAIL_VALUE = "email";
    public final static String API_MODEL_PROPERTY_STUDENTDTOUPDATE_DTO_EMAIL_DATATYPE = "String";
    public final static boolean API_MODEL_PROPERTY_STUDENTDTOUPDATE_DTO_EMAIL_REQUIRED = true;

    public final static String MODEL_STUDENT_ENTITY = "student";
    public final static String MODEL_STUDENT_TABLE = "student";

    public final static String STUDENT_NOT_FOUND_EXCEPTION = "Student not found. Please, contact the administrator for more information.";

    public final static String SWAGGER_API_DESCRIPTION = "API Documentation generated by Yeisson Kevin Vivar Rodriguez";
    public final static String SWAGGER_API_INFO_LICENSE = "YKVR";
    public final static String SWAGGER_API_INFO_TITTLE = "Challenge RxJava By Globant";
    public final static String SWAGGER_API_INFO_VERSION = "1.0";
    public final static String SWAGGER_BASE_PACKAGE = "com.challenge.rxjava.controller";
    public final static String SWAGGER_BASE_REGEX_PATH = "/student.*";

    public final static String API_OPERATION_GET_GETSTUDENT_VALUE = "getStudent";
    public final static String API_OPERATION_GET_GETSTUDENT_HTTPMETHOD = "GET";
    public final static String API_OPERATION_GET_GETSTUDENT_NOTES = "Obtener Estudiante: Deberá retornar el estudiante segùn el ID enviado, debe devolver un objeto reativo Single.";

    public final static String API_OPERATION_GET_GETALLSTUDENT_VALUE = "getAllStudent";
    public final static String API_OPERATION_GET_GETALLSTUDENT_HTTPMETHOD = "GET";
    public final static String API_OPERATION_GET_GETALLSTUDENT_NOTES = "Obtener estudiantes activos: Deberá retornar una lista de estudiantes activos en un objeto reactivo Observable.";

    public final static String API_OPERATION_POST_ADDSTUDENT_VALUE = "addStudent";
    public final static String API_OPERATION_POST_ADDSTUDENT_HTTPMETHOD = "POST";
    public final static String API_OPERATION_POST_ADDSTUDENT_NOTES = "Crear un estudiante: Este endpoint debe guardar el objeto en un repositorio y luego devolver un objeto reactivo Single que contenga los datos del estudiante guardado (ID generado al guardar el estudiante.";

    public final static String API_OPERATION_PUT_UPDATESTUDENT_VALUE = "updateStudent";
    public final static String API_OPERATION_PUT_UPDATESTUDENT_HTTPMETHOD = "PUT";
    public final static String API_OPERATION_PUT_UPDATESTUDENT_NOTES = "Actualizar un estudiante: Permite actualizar los datos de un estudiante mediante su ID, al terminar debe retornar un objeto reactivo Completable.";

    public final static String API_OPERATION_DELETE_DELETESTUDENT_VALUE = "deleteStudent";
    public final static String API_OPERATION_DELETE_DELETESTUDENT_HTTPMETHOD = "DELETE";
    public final static String API_OPERATION_DELETE_DELETESTUDENT_NOTES = "Eliminar estudiante: Deberá eliminar un estudiante del repositorio y devolver un objeto reactivo Completable.";

}
