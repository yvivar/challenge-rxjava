package com.challenge.rxjava.exception;

import com.challenge.rxjava.model.Constants;
import io.swagger.annotations.ApiModel;

@ApiModel(value = Constants.API_EXCEPTION_STUDENTNOTFOUNDEXCEPTION)
public class StudentNotFoundException extends RuntimeException {

    public StudentNotFoundException() {
        super(Constants.STUDENT_NOT_FOUND_EXCEPTION);
    }
}
