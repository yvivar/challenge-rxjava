package com.challenge.rxjava.services;

import com.challenge.rxjava.dto.StudentDtoCreate;
import com.challenge.rxjava.dto.StudentDtoUpdate;
import com.challenge.rxjava.exception.StudentNotFoundException;
import com.challenge.rxjava.model.Status;
import com.challenge.rxjava.model.Student;
import com.challenge.rxjava.repository.StudentRepository;
import io.reactivex.Completable;
import io.reactivex.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class StudentServiceImp implements IStudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public Single<Student> addStudent(StudentDtoCreate studentDto) {
        return Single.create(subscriber -> {
            subscriber.onSuccess(this.studentRepository.save(Student.getStudentCreation(studentDto)));
        });
    }

    @Override
    public Single<List<Student>> getListStudents() {
        return Single.just(Stream.of(this.studentRepository.findAll())
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .filter(x -> Status.ACTIVE.toString().equals(x.getStatus()))
                .collect(Collectors.toList())).doOnError(x -> new StudentNotFoundException());
    }

    @Override
    public Completable updateStudent(StudentDtoUpdate studentDto) {
        return Completable.create(subscriber ->
                this.studentRepository.findById(studentDto.getId()).ifPresentOrElse(
                student -> {
                    student = Student.getStudentUpdate(studentDto);
                    this.studentRepository.save(student);
                    subscriber.onComplete();
                },
                () -> subscriber.onError(new StudentNotFoundException())));
    }

    @Override
    public Completable deleteStudent(Long id) {
        return Completable.create(subscriber ->
                this.studentRepository.findById(id).ifPresentOrElse(
                        student -> {
                            student.setStatus(Status.INACTIVE.toString());
                            this.studentRepository.save(student);
                            subscriber.onComplete();
                        },
                        () -> subscriber.onError(new StudentNotFoundException())));
    }

    @Override
    public Single<Student> getStudent(Long id) {
        return Single.create(subscriber -> {
            Optional.ofNullable(this.studentRepository.findByIdAndStatus(id, Status.ACTIVE.toString()))
                    .ifPresentOrElse(subscriber::onSuccess,
                    () -> subscriber.onError(new StudentNotFoundException()));
        });
    }

}
