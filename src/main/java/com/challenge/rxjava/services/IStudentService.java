package com.challenge.rxjava.services;

import com.challenge.rxjava.dto.StudentDtoCreate;
import com.challenge.rxjava.dto.StudentDtoUpdate;
import com.challenge.rxjava.model.Student;
import io.reactivex.Completable;
import io.reactivex.Single;

import java.util.List;

public interface IStudentService {
    Single<Student> addStudent(StudentDtoCreate studentDto);
    Single<List<Student>> getListStudents();
    Completable updateStudent(StudentDtoUpdate studentDto);
    Completable deleteStudent(Long id);
    Single<Student> getStudent(Long id);
}
