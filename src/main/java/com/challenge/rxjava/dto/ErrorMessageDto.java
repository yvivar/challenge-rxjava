package com.challenge.rxjava.dto;

import com.challenge.rxjava.model.Constants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

@ApiModel(value = Constants.API_DTO_ERRORMESSAGEDTO)
@Getter
@Setter
public class ErrorMessageDto {

    @ApiModelProperty(position = Constants.API_MODEL_PROPERTY_ERRORMESSAGEDTO_DTO_STATUS_POSITION,
                      value = Constants.API_MODEL_PROPERTY_ERRORMESSAGEDTO_DTO_STATUS_VALUE,
                      dataType = Constants.API_MODEL_PROPERTY_ERRORMESSAGEDTO_DTO_STATUS_DATATYPE,
                      required = Constants.API_MODEL_PROPERTY_ERRORMESSAGEDTO_DTO_STATUS_REQUIRED)
    @NotEmpty
    private int statusCode;

    @ApiModelProperty(position = Constants.API_MODEL_PROPERTY_ERRORMESSAGEDTO_DTO_CURRENTDATE_POSITION,
                      value = Constants.API_MODEL_PROPERTY_ERRORMESSAGEDTO_DTO_CURRENTDATE_VALUE,
                      dataType = Constants.API_MODEL_PROPERTY_ERRORMESSAGEDTO_DTO_CURRENTDATE_DATATYPE,
                      required = Constants.API_MODEL_PROPERTY_ERRORMESSAGEDTO_DTO_CURRENTDATE_REQUIRED)
    @NotEmpty
    private Date currentDate;

    @ApiModelProperty(position = Constants.API_MODEL_PROPERTY_ERRORMESSAGEDTO_DTO_MESSAGEERROR_POSITION,
                      value = Constants.API_MODEL_PROPERTY_ERRORMESSAGEDTO_DTO_MESSAGEERROR_VALUE,
                      dataType = Constants.API_MODEL_PROPERTY_ERRORMESSAGEDTO_DTO_MESSAGEERROR_DATATYPE,
                      required = Constants.API_MODEL_PROPERTY_ERRORMESSAGEDTO_DTO_MESSAGEERROR_REQUIRED)
    @NotEmpty
    private String messageError;

    public ErrorMessageDto(int statusCode, Date currentDate, String messageError) {
        this.statusCode = statusCode;
        this.currentDate = currentDate;
        this.messageError = messageError;
    }
}
