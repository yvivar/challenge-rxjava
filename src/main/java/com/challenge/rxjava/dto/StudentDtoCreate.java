package com.challenge.rxjava.dto;

import com.challenge.rxjava.model.Constants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@ApiModel(value = Constants.API_DTO_STUDENTDTOCREATE)
@Getter
@Setter
public class StudentDtoCreate {

    @ApiModelProperty(position = Constants.API_MODEL_PROPERTY_STUDENTDTOCREATE_DTO_FIRSTNAME_POSITION,
                      value = Constants.API_MODEL_PROPERTY_STUDENTDTOCREATE_DTO_FIRSTNAME_VALUE,
                      dataType = Constants.API_MODEL_PROPERTY_STUDENTDTOCREATE_DTO_FIRSTNAME_DATATYPE,
                      required = Constants.API_MODEL_PROPERTY_STUDENTDTOCREATE_DTO_FIRSTNAME_REQUIRED)
    @NotEmpty
    private String firstName;

    @ApiModelProperty(position = Constants.API_MODEL_PROPERTY_STUDENTDTOCREATE_DTO_LASTNAME_POSITION,
                      value = Constants.API_MODEL_PROPERTY_STUDENTDTOCREATE_DTO_LASTNAME_VALUE,
                      dataType = Constants.API_MODEL_PROPERTY_STUDENTDTOCREATE_DTO_LASTNAME_DATATYPE,
                      required = Constants.API_MODEL_PROPERTY_STUDENTDTOCREATE_DTO_LASTNAME_REQUIRED)
    @NotEmpty
    private String lastName;

    @ApiModelProperty(position = Constants.API_MODEL_PROPERTY_STUDENTDTOCREATE_DTO_EMAIL_POSITION,
                      value = Constants.API_MODEL_PROPERTY_STUDENTDTOCREATE_DTO_EMAIL_VALUE,
                      dataType = Constants.API_MODEL_PROPERTY_STUDENTDTOCREATE_DTO_EMAIL_DATATYPE,
                      required = Constants.API_MODEL_PROPERTY_STUDENTDTOCREATE_DTO_EMAIL_REQUIRED)
    @NotEmpty
    private String email;
}
