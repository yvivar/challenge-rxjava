package com.challenge.rxjava.repository;

import com.challenge.rxjava.model.Status;
import com.challenge.rxjava.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    @Query("SELECT stu FROM student stu WHERE stu.id = :id and stu.status = :status")
    Student findByIdAndStatus(Long id, String status);

    Optional<Student> findById(Long id);
}
